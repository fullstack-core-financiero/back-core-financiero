PGDMP     #                 	    {            Core-db    15.4 (Debian 15.4-2.pgdg110+1)    15.3                0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false                       0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false                       0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false                       1262    27022    Core-db    DATABASE     u   CREATE DATABASE "Core-db" WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE_PROVIDER = libc LOCALE = 'es_ES.UTF-8';
    DROP DATABASE "Core-db";
                db_user    false                        2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
                pg_database_owner    false                       0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                   pg_database_owner    false    4            �            1259    27427    persona_juridica    TABLE     #  CREATE TABLE public.persona_juridica (
    id_persona_juridica bigint NOT NULL,
    id_usuario_financiero bigint,
    razon_social character varying(80),
    nit character varying(30),
    direccion character varying(150),
    telefono character varying(15),
    enabled boolean NOT NULL
);
 $   DROP TABLE public.persona_juridica;
       public         heap    sqluser    false    4            �            1259    27426 (   persona_juridica_id_persona_juridica_seq    SEQUENCE     �   CREATE SEQUENCE public.persona_juridica_id_persona_juridica_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ?   DROP SEQUENCE public.persona_juridica_id_persona_juridica_seq;
       public          sqluser    false    219    4                       0    0 (   persona_juridica_id_persona_juridica_seq    SEQUENCE OWNED BY     u   ALTER SEQUENCE public.persona_juridica_id_persona_juridica_seq OWNED BY public.persona_juridica.id_persona_juridica;
          public          sqluser    false    218            �            1259    27415    persona_natural    TABLE     �  CREATE TABLE public.persona_natural (
    id_persona_natural bigint NOT NULL,
    id_usuario_financiero bigint,
    nombres character varying(80) NOT NULL,
    primer_apellido character varying(80),
    segundo_apellido character varying(80) NOT NULL,
    nro_documento_identidad character varying(15) NOT NULL,
    celular character varying(20),
    direccion_domicilio character varying(100) NOT NULL,
    enabled boolean NOT NULL
);
 #   DROP TABLE public.persona_natural;
       public         heap    sqluser    false    4            �            1259    27414 &   persona_natural_id_persona_natural_seq    SEQUENCE     �   CREATE SEQUENCE public.persona_natural_id_persona_natural_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 =   DROP SEQUENCE public.persona_natural_id_persona_natural_seq;
       public          sqluser    false    217    4                       0    0 &   persona_natural_id_persona_natural_seq    SEQUENCE OWNED BY     q   ALTER SEQUENCE public.persona_natural_id_persona_natural_seq OWNED BY public.persona_natural.id_persona_natural;
          public          sqluser    false    216            �            1259    27408    usuario_financiero    TABLE     �  CREATE TABLE public.usuario_financiero (
    id_usuario_financiero bigint NOT NULL,
    fecha_creacion timestamp without time zone NOT NULL,
    usuario_creacion character varying(50) NOT NULL,
    fecha_modificacion timestamp without time zone NOT NULL,
    usuario_modificacion character varying(50) NOT NULL,
    numero_de_modificaciones bigint NOT NULL,
    enabled boolean NOT NULL
);
 &   DROP TABLE public.usuario_financiero;
       public         heap    sqluser    false    4            �            1259    27407 ,   usuario_financiero_id_usuario_financiero_seq    SEQUENCE     �   CREATE SEQUENCE public.usuario_financiero_id_usuario_financiero_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 C   DROP SEQUENCE public.usuario_financiero_id_usuario_financiero_seq;
       public          sqluser    false    215    4                       0    0 ,   usuario_financiero_id_usuario_financiero_seq    SEQUENCE OWNED BY     }   ALTER SEQUENCE public.usuario_financiero_id_usuario_financiero_seq OWNED BY public.usuario_financiero.id_usuario_financiero;
          public          sqluser    false    214            w           2604    27430 $   persona_juridica id_persona_juridica    DEFAULT     �   ALTER TABLE ONLY public.persona_juridica ALTER COLUMN id_persona_juridica SET DEFAULT nextval('public.persona_juridica_id_persona_juridica_seq'::regclass);
 S   ALTER TABLE public.persona_juridica ALTER COLUMN id_persona_juridica DROP DEFAULT;
       public          sqluser    false    218    219    219            v           2604    27418 "   persona_natural id_persona_natural    DEFAULT     �   ALTER TABLE ONLY public.persona_natural ALTER COLUMN id_persona_natural SET DEFAULT nextval('public.persona_natural_id_persona_natural_seq'::regclass);
 Q   ALTER TABLE public.persona_natural ALTER COLUMN id_persona_natural DROP DEFAULT;
       public          sqluser    false    217    216    217            u           2604    27411 (   usuario_financiero id_usuario_financiero    DEFAULT     �   ALTER TABLE ONLY public.usuario_financiero ALTER COLUMN id_usuario_financiero SET DEFAULT nextval('public.usuario_financiero_id_usuario_financiero_seq'::regclass);
 W   ALTER TABLE public.usuario_financiero ALTER COLUMN id_usuario_financiero DROP DEFAULT;
       public          sqluser    false    215    214    215                      0    27427    persona_juridica 
   TABLE DATA           �   COPY public.persona_juridica (id_persona_juridica, id_usuario_financiero, razon_social, nit, direccion, telefono, enabled) FROM stdin;
    public          sqluser    false    219   �%                 0    27415    persona_natural 
   TABLE DATA           �   COPY public.persona_natural (id_persona_natural, id_usuario_financiero, nombres, primer_apellido, segundo_apellido, nro_documento_identidad, celular, direccion_domicilio, enabled) FROM stdin;
    public          sqluser    false    217   �%                 0    27408    usuario_financiero 
   TABLE DATA           �   COPY public.usuario_financiero (id_usuario_financiero, fecha_creacion, usuario_creacion, fecha_modificacion, usuario_modificacion, numero_de_modificaciones, enabled) FROM stdin;
    public          sqluser    false    215   �&                  0    0 (   persona_juridica_id_persona_juridica_seq    SEQUENCE SET     W   SELECT pg_catalog.setval('public.persona_juridica_id_persona_juridica_seq', 1, false);
          public          sqluser    false    218                       0    0 &   persona_natural_id_persona_natural_seq    SEQUENCE SET     U   SELECT pg_catalog.setval('public.persona_natural_id_persona_natural_seq', 11, true);
          public          sqluser    false    216                       0    0 ,   usuario_financiero_id_usuario_financiero_seq    SEQUENCE SET     Z   SELECT pg_catalog.setval('public.usuario_financiero_id_usuario_financiero_seq', 1, true);
          public          sqluser    false    214            {           2606    27420 $   persona_natural persona_natural_pkey 
   CONSTRAINT     r   ALTER TABLE ONLY public.persona_natural
    ADD CONSTRAINT persona_natural_pkey PRIMARY KEY (id_persona_natural);
 N   ALTER TABLE ONLY public.persona_natural DROP CONSTRAINT persona_natural_pkey;
       public            sqluser    false    217            y           2606    27413 *   usuario_financiero usuario_financiero_pkey 
   CONSTRAINT     {   ALTER TABLE ONLY public.usuario_financiero
    ADD CONSTRAINT usuario_financiero_pkey PRIMARY KEY (id_usuario_financiero);
 T   ALTER TABLE ONLY public.usuario_financiero DROP CONSTRAINT usuario_financiero_pkey;
       public            sqluser    false    215            }           2606    27431 <   persona_juridica persona_juridica_id_usuario_financiero_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.persona_juridica
    ADD CONSTRAINT persona_juridica_id_usuario_financiero_fkey FOREIGN KEY (id_usuario_financiero) REFERENCES public.usuario_financiero(id_usuario_financiero);
 f   ALTER TABLE ONLY public.persona_juridica DROP CONSTRAINT persona_juridica_id_usuario_financiero_fkey;
       public          sqluser    false    3193    219    215            |           2606    27421 :   persona_natural persona_natural_id_usuario_financiero_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.persona_natural
    ADD CONSTRAINT persona_natural_id_usuario_financiero_fkey FOREIGN KEY (id_usuario_financiero) REFERENCES public.usuario_financiero(id_usuario_financiero);
 d   ALTER TABLE ONLY public.persona_natural DROP CONSTRAINT persona_natural_id_usuario_financiero_fkey;
       public          sqluser    false    3193    217    215                  x������ � �         �   x���=O�0@�˯���?�ԣ$���u��Vkɵ��V�_�]/�z��Q�mAy�Hnb8�����x�-��%���Hzն��FHP�Qn�:q*�OF�H�dy	��t������w�*�c�ɭ7#t�k:gG|C�KZ��������sږ<Y?���_�����4�3��9b>�w��H?��C?d����p@��ڸ��Vb���_�&��|S�/���_򲐧u�`�����&�         B   x�3�4202�54�54V02�24�25�334775�LL���sH�/J�M���K��%Q�g	W� %�     