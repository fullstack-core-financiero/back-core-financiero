package com.core.web.api.backcore.repository;

import com.core.web.api.backcore.entity.PersonaJuridica;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonaJuridicaRepository extends JpaRepository<PersonaJuridica, Long> {
}
