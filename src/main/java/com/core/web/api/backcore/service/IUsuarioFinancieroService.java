package com.core.web.api.backcore.service;

import com.core.web.api.backcore.entity.UsuarioFinanciero;

import java.util.List;
import java.util.Optional;
public interface IUsuarioFinancieroService {
    List<UsuarioFinanciero> findAll();
    Optional<UsuarioFinanciero> findByID(Long id);
    UsuarioFinanciero save(UsuarioFinanciero newUsuarioFinanciero);
    UsuarioFinanciero update(Long id, UsuarioFinanciero updUsuarioFinanciero);
    void delete(Long id);
}
