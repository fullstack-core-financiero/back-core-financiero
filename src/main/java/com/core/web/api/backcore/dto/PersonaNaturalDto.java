package com.core.web.api.backcore.dto;

import com.core.web.api.backcore.entity.UsuarioFinanciero;
import lombok.Data;

@Data
public class PersonaNaturalDto {

    private long id;
    private UsuarioFinanciero idUsuarioFinanciero;
    private String nombres;
    private String primerApellido;
    private String segundoApellido;
    private String numeroDocumentoIdentidad;
    private String celular;
    private String direccionDomicilio;
    private boolean enabled;
}
