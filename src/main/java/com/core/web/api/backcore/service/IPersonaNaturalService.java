package com.core.web.api.backcore.service;

import com.core.web.api.backcore.entity.PersonaNatural;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface IPersonaNaturalService {

    List<PersonaNatural> getAll(String palabraClave);
    Page<PersonaNatural> getAllPageable(Pageable pageable);
    Optional<PersonaNatural> getByID(Long id);
    PersonaNatural save(PersonaNatural newPersonaNatural);
    PersonaNatural update(Long id, PersonaNatural updPersonaNatural);
    void delete(Long id);
}
