package com.core.web.api.backcore.dto;

import com.core.web.api.backcore.entity.UsuarioFinanciero;
import lombok.Data;

@Data
public class PersonaJuridicaDto {

    private long id;
    private UsuarioFinanciero idUsuarioFinanciero;
    private String razonSocial;
    private String nit;
    private String direccion;
    private String telefono;
    private boolean enabled;
}
