package com.core.web.api.backcore;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BackCoreFinancieroApplication {

	public static void main(String[] args) {
		SpringApplication.run(BackCoreFinancieroApplication.class, args);
	}

}
