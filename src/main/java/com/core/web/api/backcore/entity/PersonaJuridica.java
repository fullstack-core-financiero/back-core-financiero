package com.core.web.api.backcore.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "persona_juridica")
public class PersonaJuridica {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_persona_juridica", nullable = false, unique = true)
    private long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_usuario_financiero", referencedColumnName = "id_usuario_financiero")
    @JsonBackReference(value = "idUsuarioFinanciero")
    private UsuarioFinanciero idUsuarioFinanciero;

    @Column(name = "razon_social", length = 80)
    private String razonSocial;

    @Column(length = 30)
    private String nit;

    @Column(length = 150)
    private String direccion;

    @Column(length = 15)
    private String telefono;

    @Column(nullable = false)
    private boolean enabled;
}
