package com.core.web.api.backcore.repository;

import com.core.web.api.backcore.entity.UsuarioFinanciero;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsuarioFinancieroRepository extends JpaRepository<UsuarioFinanciero, Long> {
}
