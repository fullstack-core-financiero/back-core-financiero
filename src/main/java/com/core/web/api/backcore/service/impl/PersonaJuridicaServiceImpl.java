package com.core.web.api.backcore.service.impl;

import com.core.web.api.backcore.entity.PersonaJuridica;
import com.core.web.api.backcore.repository.PersonaJuridicaRepository;
import com.core.web.api.backcore.service.IPersonaJuridicaService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class PersonaJuridicaServiceImpl implements IPersonaJuridicaService {

    private final PersonaJuridicaRepository pJuridicaRepository;
    @Override
    @Transactional(readOnly = true)
    public List<PersonaJuridica> findAll() {
        return pJuridicaRepository.findAll();
    }
    @Override
    @Transactional(readOnly = true)
    public Optional<PersonaJuridica> findByID(Long id) {
        return pJuridicaRepository.findById(id);
    }
    @Override
    @Transactional
    public PersonaJuridica save(PersonaJuridica newPersonaJuridica) {
        return pJuridicaRepository.save(newPersonaJuridica);
    }
    @Override
    @Transactional
    public PersonaJuridica update(Long id, PersonaJuridica updPersonaJuridica) {

        var updatedPersonaJuridica = pJuridicaRepository.findById(id);

        if (updatedPersonaJuridica.isPresent()) {
            updatedPersonaJuridica.get().setRazonSocial(updPersonaJuridica.getRazonSocial());
            updatedPersonaJuridica.get().setNit(updPersonaJuridica.getNit());
            updatedPersonaJuridica.get().setDireccion(updPersonaJuridica.getDireccion());
            updatedPersonaJuridica.get().setTelefono(updPersonaJuridica.getTelefono());
            updatedPersonaJuridica.get().setEnabled(updPersonaJuridica.isEnabled());
        }
        return pJuridicaRepository.save(updatedPersonaJuridica.get());
    }
    @Override
    @Transactional
    public void delete(Long id) {
        pJuridicaRepository.deleteById(id);
    }
}
