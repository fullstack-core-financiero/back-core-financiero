package com.core.web.api.backcore.dto;

import lombok.Data;
import java.time.LocalDateTime;
@Data
public class UsuarioFinancieroDto {

    private Long id;
    private boolean enabled;
    private LocalDateTime fechaCreacion;
    private String usuarioCreacion;
    private LocalDateTime fechaModificacion;
    private String usuarioModificacion;
    private long numeroDeModificacion;
}
