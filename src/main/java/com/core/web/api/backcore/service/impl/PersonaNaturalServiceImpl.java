package com.core.web.api.backcore.service.impl;

import com.core.web.api.backcore.entity.PersonaNatural;
import com.core.web.api.backcore.repository.PersonaNaturalRepository;
import com.core.web.api.backcore.repository.UsuarioFinancieroRepository;
import com.core.web.api.backcore.service.IPersonaNaturalService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class PersonaNaturalServiceImpl implements IPersonaNaturalService {

    private final PersonaNaturalRepository pNaturalRepository;
    private final UsuarioFinancieroRepository usuarioFinancieroRepository;
    @Override
    @Transactional(readOnly = true)
    public List<PersonaNatural> getAll(String palabraClave) {
        if (palabraClave != null) {
            return pNaturalRepository.search(palabraClave);
        }
        return pNaturalRepository.findAll();
    }
    @Override
    @Transactional(readOnly = true)
    public Page<PersonaNatural> getAllPageable(Pageable pageable) {
        return pNaturalRepository.findAll(pageable);
    }
    @Override
    @Transactional(readOnly = true)
    public Optional<PersonaNatural> getByID(Long id) {
        return pNaturalRepository.findById(id);
    }
    @Override
    @Transactional
    public PersonaNatural save(PersonaNatural newPersonaNatural) {
        return pNaturalRepository.save(newPersonaNatural);
    }
    @Override
    @Transactional
    public PersonaNatural update(Long id, PersonaNatural updPersonaNatural) {

        var updatedPersonaNatural = pNaturalRepository.findById(id);

        if (updatedPersonaNatural.isPresent()) {
            updatedPersonaNatural.get().setIdUsuarioFinanciero(updPersonaNatural.getIdUsuarioFinanciero());
            updatedPersonaNatural.get().setNombres(updPersonaNatural.getNombres());
            updatedPersonaNatural.get().setPrimerApellido(updPersonaNatural.getPrimerApellido());
            updatedPersonaNatural.get().setSegundoApellido(updPersonaNatural.getSegundoApellido());
            updatedPersonaNatural.get().setNumeroDocumentoIdentidad(updPersonaNatural.getNumeroDocumentoIdentidad());
            updatedPersonaNatural.get().setCelular(updPersonaNatural.getCelular());
            updatedPersonaNatural.get().setDireccionDomicilio(updPersonaNatural.getDireccionDomicilio());
            updatedPersonaNatural.get().setEnabled(updPersonaNatural.isEnabled());
        }
        return pNaturalRepository.save(updatedPersonaNatural.get());
    }
    @Override
    @Transactional
    public void delete(Long id) {
        pNaturalRepository.deleteById(id);
    }
}
