package com.core.web.api.backcore.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "persona_natural")
public class PersonaNatural {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_persona_natural", nullable = false, unique = true)
    private long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_usuario_financiero", referencedColumnName = "id_usuario_financiero")
    @JsonBackReference(value = "idUsuarioFinanciero")
    private UsuarioFinanciero idUsuarioFinanciero;

    @Column(length = 80)
    private String nombres;

    @Column(name = "primer_apellido", length = 80)
    private String primerApellido;

    @Column(name = "segundo_apellido", length = 80)
    private String segundoApellido;

    @Column(name = "nro_documento_identidad", length = 15)
    private String numeroDocumentoIdentidad;

    @Column(length = 20)
    private String celular;

    @Column(name = "direccion_domicilio", length = 100)
    private String direccionDomicilio;

    @Column(nullable = false)
    private boolean enabled;
}
