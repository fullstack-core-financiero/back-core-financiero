package com.core.web.api.backcore.service;

import com.core.web.api.backcore.entity.PersonaJuridica;

import java.util.List;
import java.util.Optional;

public interface IPersonaJuridicaService {

    List<PersonaJuridica> findAll();
    Optional<PersonaJuridica> findByID(Long id);
    PersonaJuridica save(PersonaJuridica newPersonaJuridica);
    PersonaJuridica update(Long id, PersonaJuridica updPersonaJuridica);
    void delete(Long id);
}
