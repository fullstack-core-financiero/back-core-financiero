package com.core.web.api.backcore.controller;

import com.core.web.api.backcore.dto.UsuarioFinancieroDto;
import com.core.web.api.backcore.entity.UsuarioFinanciero;
import com.core.web.api.backcore.service.IUsuarioFinancieroService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/v1/core")
@RequiredArgsConstructor
@Slf4j
public class UsuarioFinancieroController {

    private final IUsuarioFinancieroService usuarioService;
    private final ModelMapper modelMapper = new ModelMapper();

    @GetMapping("/usuarios")
    public ResponseEntity<List<UsuarioFinancieroDto>> obtenerUsuarios() {
        try {
            log.debug("Listado completo de registros");
            return new ResponseEntity<>(usuarioService.findAll().stream().map(
                            usuarios->modelMapper.map(usuarios, UsuarioFinancieroDto.class))
                    .collect(Collectors.toList()), HttpStatus.OK);
        }
        catch (DataAccessException daex) {
            log.error("Error al obtener el listado " + daex.getMostSpecificCause());
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/usuarios/{id}")
    public ResponseEntity<UsuarioFinancieroDto> obtenerUsuarioPorID(@Valid @PathVariable Long id) {

        try{
            var usuario = usuarioService.findByID(id);
            var usuarioResponse = modelMapper.map(usuario, UsuarioFinancieroDto.class);

            if (usuarioResponse == null) {
                log.error("El objeto se encuentra vacío, favor verifique! ");
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
            else {
                log.debug("Un registro");
                return new ResponseEntity<>(usuarioResponse, HttpStatus.OK);
            }
        }
        catch (DataAccessException daex) {
            log.error("Error al obtener un registro " + daex.getMostSpecificCause());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/usuarios")
    public ResponseEntity<?> saveUsuario(@Valid @RequestBody UsuarioFinancieroDto nuevoUsuarioFinancieroDto) {

        try {
            var usuarioRequest = modelMapper.map(nuevoUsuarioFinancieroDto, UsuarioFinanciero.class);
            var nuevoUsuario = usuarioService.save(usuarioRequest);
            var usuarioResponse = modelMapper.map(nuevoUsuario, UsuarioFinancieroDto.class);

            if (usuarioResponse == null) {
                log.error("El objeto se encuentra vacio, favor verifique! ");
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
            else {
                log.debug("Registro guardado! ");
                return new ResponseEntity<>(usuarioResponse, HttpStatus.CREATED);
            }
        }
        catch (DataAccessException daex) {
            log.error("Error al guardar el rgistro " + daex.getMostSpecificCause());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/usuarios/{id}")
    public ResponseEntity<UsuarioFinancieroDto> updateUsuario(@Valid @PathVariable Long id,
                                                              @Valid @RequestBody UsuarioFinancieroDto updateUsuarioDto) {

        try {
            var usuarioRequest = modelMapper.map(updateUsuarioDto, UsuarioFinanciero.class);
            var usuarioUpdated = usuarioService.update(id, usuarioRequest);
            var usuarioResponse = modelMapper.map(usuarioUpdated, UsuarioFinancieroDto.class);

            if (usuarioResponse == null) {
                log.error("El objeto se encuentra vacio, favor verifique! ");
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
            else {
                log.debug("Registro actualizado!");
                return new ResponseEntity<>(usuarioResponse, HttpStatus.ACCEPTED);
            }
        }
        catch (DataAccessException daex) {
            log.error("Error al actualizar el registro " + daex.getMostSpecificCause());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/usuarios/{id}")
    public ResponseEntity<?> deleteUsuario(@Valid @PathVariable Long id) {

        try {
            usuarioService.delete(id);
            log.debug("Registro eliminado! ");
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        catch (DataAccessException daex) {
            log.error("Error al actualizar el registro " + daex.getMostSpecificCause());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
