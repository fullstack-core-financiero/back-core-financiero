package com.core.web.api.backcore.controller;

import com.core.web.api.backcore.dto.PersonaNaturalDto;
import com.core.web.api.backcore.entity.PersonaNatural;
import com.core.web.api.backcore.service.IPersonaNaturalService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/v1/core")
@RequiredArgsConstructor
@Slf4j
public class PersonaNaturalController {

    private final IPersonaNaturalService personaService;
    private final ModelMapper modelMapper = new ModelMapper();

    @GetMapping("/personas")
    public ResponseEntity<List<PersonaNaturalDto>> obtenerPersonas(String palabraClave) {

        try {
            log.debug("Listado completo de registros");
            return new ResponseEntity<>(personaService.getAll(palabraClave).stream().map(
                    personas->modelMapper.map(personas, PersonaNaturalDto.class))
                    .collect(Collectors.toList()), HttpStatus.OK);
        }
        catch (DataAccessException daex) {
            log.error("Error al obtener el listado " + daex.getMostSpecificCause());
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/personas/pageable")
    public ResponseEntity<Page<PersonaNatural>> obtenerPaginacionPersonas(@RequestParam(required = false, defaultValue = "0") Integer page,
                                                                          @RequestParam(required = false, defaultValue = "10") Integer size,
                                                                          @RequestParam(required = false, defaultValue = "nombres") String order,
                                                                          @RequestParam(required = false, defaultValue = "true")Boolean asc) {
        try {
            log.debug("Paginación de registros");
            var paginacionPersonas = personaService.getAllPageable(PageRequest.of(page, size, Sort.by(order)));

            if (!asc) {
                paginacionPersonas = personaService.getAllPageable(PageRequest.of(page, size, Sort.by(order).descending()));
            }
            return new ResponseEntity<>(paginacionPersonas, HttpStatus.OK);
        }
        catch (DataAccessException daex) {
            log.error("Error al obtener la paginación " + daex.getMostSpecificCause());
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/personas/{id}")
    public ResponseEntity<PersonaNaturalDto> obtenerPersonaPorID(@Valid @PathVariable Long id) {

        try{
            var persona = personaService.getByID(id);
            var personaResponse = modelMapper.map(persona, PersonaNaturalDto.class);

            if (personaResponse == null) {
                log.error("El objeto se encuentra vacío, favor verifique! ");
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
            else {
                log.debug("Un registro");
                return new ResponseEntity<>(personaResponse, HttpStatus.OK);
            }
        }
        catch (DataAccessException daex) {
            log.error("Error al obtener un registro " + daex.getMostSpecificCause());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/personas")
    public ResponseEntity<?> savePersonaNatural(@Valid @RequestBody PersonaNaturalDto nuevaPersonaDto) {

        try {
            var personaRequest = modelMapper.map(nuevaPersonaDto, PersonaNatural.class);
            var nuevaPersona = personaService.save(personaRequest);
            var personaResponse = modelMapper.map(nuevaPersona, PersonaNaturalDto.class);

            if (personaResponse == null) {
                log.error("El objeto se encuentra vacio, favor verifique! ");
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
            else {
                log.debug("Registro guardado! ");
                return new ResponseEntity<>(personaResponse, HttpStatus.CREATED);
            }
        }
        catch (DataAccessException daex) {
            log.error("Error al guardar el rgistro " + daex.getMostSpecificCause());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/personas/{id}")
    public ResponseEntity<PersonaNaturalDto> updatePersonaNatural(@Valid @PathVariable Long id,
                                                            @Valid @RequestBody PersonaNaturalDto updatePersonaDto) {

        try {
            var personaRequest = modelMapper.map(updatePersonaDto, PersonaNatural.class);
            var personaUpdated = personaService.update(id, personaRequest);
            var personaResponse = modelMapper.map(personaUpdated, PersonaNaturalDto.class);

            if (personaResponse == null) {
                log.error("El objeto se encuentra vacio, favor verifique! ");
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
            else {
                log.debug("Registro actualizado!");
                return new ResponseEntity<>(personaResponse, HttpStatus.ACCEPTED);
            }
        }
        catch (DataAccessException daex) {
            log.error("Error al actualizar el registro " + daex.getMostSpecificCause());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/personas/{id}")
    public ResponseEntity<?> deletePersona(@Valid @PathVariable Long id) {

        try {
            personaService.delete(id);
            log.debug("Registro eliminado! ");
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        catch (DataAccessException daex) {
            log.error("Error al actualizar el registro " + daex.getMostSpecificCause());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
