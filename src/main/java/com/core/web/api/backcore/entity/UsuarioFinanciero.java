package com.core.web.api.backcore.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.time.LocalDateTime;
import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "usuario_financiero")
public class UsuarioFinanciero {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_usuario_financiero", nullable = false, unique = true)
    private Long id;

    @Column(nullable = false)
    private boolean enabled;

    @CreatedDate
    @Column(name = "fecha_creacion", nullable = false)
    private LocalDateTime fechaCreacion;

    @CreatedBy
    @Column(name = "usuario_creacion", length = 50, nullable = false)
    private String usuarioCreacion;

    @LastModifiedDate
    @Column(name = "fecha_modificacion", nullable = false)
    private LocalDateTime fechaModificacion;

    @LastModifiedBy
    @Column(name = "usuario_modificacion", length = 50, nullable = false)
    private String usuarioModificacion;

    @Version
    @Column(name = "numero_de_modificaciones", nullable = false)
    private long numeroDeModificacion;
}
