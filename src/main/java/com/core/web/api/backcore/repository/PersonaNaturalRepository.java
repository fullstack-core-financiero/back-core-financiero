package com.core.web.api.backcore.repository;

import com.core.web.api.backcore.entity.PersonaNatural;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface PersonaNaturalRepository extends JpaRepository<PersonaNatural, Long> {

    List<PersonaNatural> findAll();

    @Query("SELECT p FROM PersonaNatural p WHERE p.nombres LIKE %?1% " +
            "OR p.primerApellido LIKE %?1% " +
            "OR p.segundoApellido LIKE %?1%")
    List<PersonaNatural> search(String palabraClave);
}
