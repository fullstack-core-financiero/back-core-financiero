package com.core.web.api.backcore.service.impl;

import com.core.web.api.backcore.entity.UsuarioFinanciero;
import com.core.web.api.backcore.repository.UsuarioFinancieroRepository;
import com.core.web.api.backcore.service.IUsuarioFinancieroService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class UsuarioFinancieroServiceImpl implements IUsuarioFinancieroService {

    private final UsuarioFinancieroRepository usuarioRepository;

    @Override
    @Transactional(readOnly = true)
    public List<UsuarioFinanciero> findAll() {
        return usuarioRepository.findAll();
    }
    @Override
    @Transactional(readOnly = true)
    public Optional<UsuarioFinanciero> findByID(Long id) {
        return usuarioRepository.findById(id);
    }
    @Override
    @Transactional
    public UsuarioFinanciero save(UsuarioFinanciero newUsuarioFinanciero) {
        return usuarioRepository.save(newUsuarioFinanciero);
    }
    @Override
    @Transactional
    public UsuarioFinanciero update(Long id, UsuarioFinanciero updUsuarioFinanciero) {

        var updatedUsuario = usuarioRepository.findById(id);
        updatedUsuario.ifPresent(updUsuaro->updatedUsuario.get().setEnabled(updUsuarioFinanciero.isEnabled()));
        return usuarioRepository.save(updatedUsuario.get());
    }
    @Override
    @Transactional
    public void delete(Long id) {
        usuarioRepository.deleteById(id);
    }
}
